# Network Security Lab 2021

For this lab it's possible to decide to run the lab using the provided ```.ova``` file
or spinning up the docker containers in your native environment

## Prepare the lab in native environment

It's possible to run this lab using docker in your native environment

First clone this repository then ```cd``` in cloned folder

```
git clone https://gitlab.com/mattia.simone97/network-security-lab-2021.git
cd network-security-lab-2021
```

Be sure that docker it's correctly configured to run Elasticsearch, you can follow a guide in the [Wazuh documentation](https://documentation.wazuh.com/current/docker/wazuh-container.html) according to your OS

### Configure Cortex

Create  ```application.conf```

```
cp cortex/application.sample cortex/application.conf
```

Edit the configuration inserting a secret key

### Configure TheHive

Create ```application.conf```

```
cp thehive/application.sample thehive/application.conf
```

Edit the configuration inserting a secret key

### Run the containers

To run the containers run, please be sure that the docker daemon is up and running

```
docker-compose up -d
```

Follow the instructions for migrating the database and creating the first user on the admin GUI on both Cortex and TheHive.

Wazuh it's already configured, you can signin using ```admin/admin```

### Connect TheHive with Cortex

It's possible to connect TheHive to Cortex and perform action on Cortex directly from
the GUI of TheHive.

You will need to create a Cortex API key first

1. Create an organization
2. Create an user for that organization (with admin privileges)
3. Create a password and an API key

As reference you can check the [documentation of TheHive](https://blog.agood.cloud/posts/2019/09/27/integrate-thehive-and-cortex/)

Look after fot this snippet and insert the API key into ```application.conf``` of the TheHive

```
cortex {
  "CORTEX-SERVER-ID" {
    # URL of the Cortex server
    url = "http://cortex:9001"
    # Key of the Cortex user, mandatory for Cortex 2
    key = "*****INSERT CORTEX API KEY HERE*****"
  }
```

Finally restart TheHive

```
docker restart thehive
```

### Install Cortex Analyzers

To install cortex analyzer first clone the Cortex-Analyzers repository inside ```cortex``` folder (you might remove the existing ```Cortex-Analyzers``` folder)

```
git clone https://github.com/TheHive-Project/Cortex-Analyzers.git
```

```
docker exec -it cortex /bin/bash
/opt/install_cortex_analyzers.sh
```

If the script doesn't work please restart cortex container first then rerun

```
docker restart cortex
```

### Install wazuh2thehive

To pass alert from Wazuh to the hive we use a custom integratation for Wazuh

Inside ```wazuh2thehive``` in wazuh-manager.conf look for this snippet and add an API key from an user with **alert permission** on TheHive

```
<integration>
  <name>custom-w2thive</name>
  <hook_url>http://thehive:9000</hook_url>
  <api_key></api_key>
  <alert_format>json</alert_format>
</integration>
```
To install the integration run:

```
docker exec -it wazuh /bin/bash
/opt/wazuh2thehive/install-wazuh2thehive.sh
```

#### Reference for GUIs

- TheHive: http://localhost:9000
- Cortex: http://localhost:9001
- Wazuh (through Kibana): https://localhost
  + credentials: ```admin/admin```

## Use the prepared VM

Download the appropriate ```.ova```

- [VirtualBox](https://drive.google.com/file/d/1sk-JE_P2hN-cPRDqWfabRg3eAYhZ0sE-/view?usp=sharing)
- [VMWare](https://drive.google.com/file/d/1rjDn3I--4FWDolrD-uoBHr-iZVpuJbin/view?usp=sharing)

Import the VM, assign to the VM at least 6GB of memory otherwise Elasticsearch will not work

Login into Ubuntu Server with ```lab/admin``` then get the ip address of your VM using
```ifconfig```

you can now open a terminal in your native environment and ```ssh``` into the vm

```
ssh lab@IP_VM_HERE
```
Inside the VM

```
cd network-security-lab-2021
docker-compose up -d
docker exec -it wazuh /bin/bash
/opt/wazuh2thehive/install-wazuh2thehive.sh
```

#### Reference for GUIs

- TheHive: http://IP_ADDR_VM:9000
  + credentials: ```admin/admin```
- Cortex: http://IP_ADDR_VM:9001
  + credentials: ```admin/admin``` for superadmin
  + credentials: ```lab-admin/admin``` for testing org lab
- Wazuh (through Kibana): https://IP_ADDR_VM
  + credentials: ```admin/admin```
