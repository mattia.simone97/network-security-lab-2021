## SECRET KEY
#
# The secret key is used to secure cryptographic functions.
#
# IMPORTANT: If you deploy your application to several  instances,  make
# sure to use the same key.
play.http.secret.key="*****INSERT SECRET HERE*****"

# Elasticsearch
search {
  ## Basic configuration
  # Index name.
  index = the_hive
  # ElasticSearch instance address.
  uri = "http://elasticsearch-thehive:9200/"

  # Scroll keepalive
  keepalive = 1m
  # Size of the page for scroll
  pagesize = 50
  # Number of shards
  nbshards = 5
  # Number of replicas
  nbreplicas = 1
  # Arbitrary settings
  settings {
    # Maximum number of nested fields
    mapping.nested_fields.limit = 100
  }

  ## Authentication configuration
  #search.username = ""
  #search.password = ""

  ## SSL configuration
  #search.keyStore {
  #  path = "/path/to/keystore"
  #  type = "JKS" # or PKCS12
  #  password = "keystore-password"
  #}
  #search.trustStore {
  #  path = "/path/to/trustStore"
  #  type = "JKS" # or PKCS12
  #  password = "trustStore-password"
  #}
 }

 # Datastore
datastore {
  name = data
  # Size of stored data chunks
  chunksize = 50k
  hash {
    # Main hash algorithm /!\ Don't change this value
    main = "SHA-256"
    # Additional hash algorithms (used in attachments)
    extra = ["SHA-1", "MD5"]
  }
  attachment.password = "malware"
}

auth {
  # "provider" parameter contains authentication provider. It can be multi-valued (useful for migration)
  # available auth types are:
  # services.LocalAuthSrv : passwords are stored in user entity (in Elasticsearch). No configuration is required.
  # ad : use ActiveDirectory to authenticate users. Configuration is under "auth.ad" key
  # ldap : use LDAP to authenticate users. Configuration is under "auth.ldap" key
  # oauth2 : use OAuth/OIDC to authenticate users. Configuration is under "auth.oauth2" and "auth.sso" keys
  provider = [local]

  # By default, basic authentication is disabled. You can enable it by setting "method.basic" to true.
  #method.basic = true

  ad {
    # The Windows domain name in DNS format. This parameter is required if you do not use
    # 'serverNames' below.
    #domainFQDN = "mydomain.local"

    # Optionally you can specify the host names of the domain controllers instead of using 'domainFQDN
    # above. If this parameter is not set, TheHive uses 'domainFQDN'.
    #serverNames = [ad1.mydomain.local, ad2.mydomain.local]

    # The Windows domain name using short format. This parameter is required.
    #domainName = "MYDOMAIN"

    # If 'true', use SSL to connect to the domain controller.
    #useSSL = true
  }

  ldap {
    # The LDAP server name or address. The port can be specified using the 'host:port'
    # syntax. This parameter is required if you don't use 'serverNames' below.
    #serverName = "ldap.mydomain.local:389"

    # If you have multiple LDAP servers, use the multi-valued setting 'serverNames' instead.
    #serverNames = [ldap1.mydomain.local, ldap2.mydomain.local]

    # Account to use to bind to the LDAP server. This parameter is required.
    #bindDN = "cn=thehive,ou=services,dc=mydomain,dc=local"

    # Password of the binding account. This parameter is required.
    #bindPW = "***secret*password***"

    # Base DN to search users. This parameter is required.
    #baseDN = "ou=users,dc=mydomain,dc=local"

    # Filter to search user in the directory server. Please note that {0} is replaced
    # by the actual user name. This parameter is required.
    #filter = "(cn={0})"

    # If 'true', use SSL to connect to the LDAP directory server.
    #useSSL = true
  }

  oauth2 {
    # URL of the authorization server
    #clientId = "client-id"
    #clientSecret = "client-secret"
    #redirectUri = "https://my-thehive-instance.example/api/ssoLogin"
    #responseType = "code"
    #grantType = "authorization_code"

    # URL from where to get the access token
    #authorizationUrl = "https://auth-site.com/OAuth/Authorize"
    #tokenUrl = "https://auth-site.com/OAuth/Token"

    # The endpoint from which to obtain user details using the OAuth token, after successful login
    #userUrl = "https://auth-site.com/api/User"
    #scope = ["openid profile"]
  }

  # Single-Sign On
  sso {
    # Autocreate user in database?
    #autocreate = false

    # Autoupdate its profile and roles?
    #autoupdate = false

    # Autologin user using SSO?
    #autologin = false
    # Attributes mappings
    #attributes {
    #  login = "sub"
    #  name = "name"
    #  groups = "groups"
    #  #roles = "roles"
    #}

    # Name of mapping class from user resource to backend user ('simple' or 'group')
    #mapper = group
    # Default roles for users with no groups mapped ("read", "write", "admin")
    #defaultRoles = []

    #groups {
    #  # URL to retrieve groups (leave empty if you are using OIDC)
    #  #url = "https://auth-site.com/api/Groups"
    #  # Group mappings, you can have multiple roles for each group: they are merged
    #  mappings {
    #    admin-profile-name = ["admin"]
    #    editor-profile-name = ["write"]
    #    reader-profile-name = ["read"]
    #  }
    #}
  }
}

# Maximum time between two requests without requesting authentication
session {
  warning = 5m
  inactivity = 1h
}

# Streaming
stream.longpolling {
  # Maximum time a stream request waits for new element
  refresh = 1m
  # Lifetime of the stream session without request
  cache = 15m
  nextItemMaxWait = 500ms
  globalMaxWait = 1s
}

## Enable the Cortex module
play.modules.enabled += connectors.cortex.CortexConnector

cortex {
  "CORTEX-SERVER-ID" {
    # URL of the Cortex server
    url = "http://cortex:9001"
    # Key of the Cortex user, mandatory for Cortex 2
    key = "*****INSERT CORTEX API KEY HERE*****"
  }
  # HTTP client configuration, more details in section 8
  # ws {
  #   proxy {}
  #   ssl {}
  # }
  # Check job update time interval
  refreshDelay = 1 minute
  # Maximum number of successive errors before give up
  maxRetryOnError = 3
  # Check remote Cortex status time interval
  statusCheckInterval = 1 minute
}
