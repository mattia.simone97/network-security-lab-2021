#!/bin/bash

# helpFunction()
# {
#    echo ""
#    echo "Usage: $0 -k apiKey"
#    exit 1
# }
#
# while getopts "k:" opt
# do
#    case "$opt" in
#       k ) key="$OPTARG" ;;
#       ? ) helpFunction ;;
#    esac
# done
#
# # Print helpFunction in case parameters are empty
# if [ -z "$key" ]
# then
#    echo "Some or all of the parameters are empty";
#    helpFunction
# fi

#(THEHIVE_API_KEY=$key &&
#envsubst '$THEHIVE_API_KEY' < "wazuh-manager.conf" > "ossec.conf" &&
(/var/ossec/framework/python/bin/pip3 install -r /opt/wazuh2thehive/requirements.txt &&
cp /opt/wazuh2thehive/custom-w2thive.py /var/ossec/integrations/custom-w2thive.py &&
cp /opt/wazuh2thehive/custom-w2thive /var/ossec/integrations/custom-w2thive &&
cp /opt/wazuh2thehive/wazuh-manager.conf /var/ossec/etc/ossec.conf &&
chmod 755 /var/ossec/integrations/custom-w2thive.py &&
chmod 755 /var/ossec/integrations/custom-w2thive &&
chown root:ossec /var/ossec/integrations/custom-w2thive.py &&
chown root:ossec /var/ossec/integrations/custom-w2thive &&
/var/ossec/bin/ossec-control restart && echo "[wazuh2thehive] DONE") || echo "[wazuh2thehive] ERROR"
